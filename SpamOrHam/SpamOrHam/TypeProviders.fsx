#r @"../../packages/FSharp.Data/lib/net45/FSharp.Data.dll"

open FSharp.Data

type Questions = JsonProvider<"""https://api.stackexchange.com/2.2/questions?site=stackoverflow""">

let csQuestions = """https://api.stackexchange.com/2.2/questions?site=stackoverflow&tagged=C%23"""

Questions.Load(csQuestions).Items
|> Seq.iter (fun q -> printfn "%s" q.Title)

let questionQuery = """https://api.stackexchange.com/2.2/questions?site=stackoverflow"""

let tagged tags query =
  let joinedTags = tags |> String.concat ";"
  sprintf "%s&tagged=%s" query joinedTags

let page p query = sprintf "%s&page=%i" query p

let pageSize s query = sprintf "%s&pagesize=%i" query s

let extractQuestions (query:string) = Questions.Load(query).Items

let ``C#`` = "C%23"
let ``F#`` = "F%23"

let fsSample =
  questionQuery
  |> tagged [``F#``]
  |> pageSize 100
  |> extractQuestions

let csSample =
  questionQuery
  |> tagged [``C#``]
  |> pageSize 100
  |> extractQuestions

let analyzeTags (qs:Questions.Item seq) =
  qs
  |> Seq.collect (fun questions -> questions.Tags)
  |> Seq.countBy id
  |> Seq.filter (fun (_,count) -> count > 2)
  |> Seq.sortBy (fun (_,count) -> -count)
  |> Seq.iter (fun (tag,count) -> printfn "%s,%i" tag count)

analyzeTags fsSample
analyzeTags csSample

let wb = WorldBankData.GetDataContext()

let countries = wb.Countries
let pop2000 = [ for c in countries -> c.Indicators.``Population, total``.[2000] ]
let pop2010 = [ for c in countries -> c.Indicators.``Population, total``.[2010] ]

#r @"../../packages/R.NET.Community/lib/net40/RDotNet.dll"
#r @"../../packages/RProvider/lib/net40/RProvider.Runtime.dll"
#r @"../../packages/RProvider/lib/net40/RProvider.dll"

open RProvider
open RProvider.``base``
open RProvider.graphics

let surface = [ for c in countries -> c.Indicators.``Surface area (sq. km)``.[2010] ]
R.summary(surface) |> R.print
R.hist(surface |> R.log)
R.plot(surface, pop2010)

let pollution = [ for c in countries -> c.Indicators.``CO2 emissions (kt)``.[2000] ]
let education = [ for c in countries -> c.Indicators.``School enrollment, secondary, male (% gross)``.[2000] ]

let rdf =
  [ "Pop2000", box pop2000
    "Pop2010", box pop2010
    "Surface", box surface
    "Pollution", box pollution
    "Education", box education ]
  |> namedParams
  |> R.data_frame

rdf |> R.plot
rdf |> R.summary |> R.print

#r @"../../packages/Deedle/lib/net40/Deedle.dll"
open Deedle

let series1 = series [ "Alpha", 1.; "Bravo", 2.; "Delta", 4.; ]
let series2 = series [ "Bravo", 20.; "Charlie", 30.; "Delta", 40.; ]
let toyFrame = frame [ "First", series1; "Second", series2; ]

series1 |> Stats.sum
series2 |> Stats.mean
toyFrame?Second |> Stats.mean

toyFrame?New <- toyFrame?First + toyFrame?Second
toyFrame |> Stats.mean